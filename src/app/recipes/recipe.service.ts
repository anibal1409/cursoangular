import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';


@Injectable()
export class RecipeService {
  recipeChanged = new Subject<Recipe[]>();
  /*private recipes: Recipe[] = [
    new Recipe('Hallaca',
    'Descripcion',
    'http://s3.amazonaws.com/gmi-digital-library/cc3c2b09-7e40-485e-af2c-fa1acc9f454a.jpg',
    [
      new Ingredient('Cochino', 1),
      new Ingredient('Carne de res', 1),
      new Ingredient('Cebolla', 1),
      new Ingredient('Cebollin', 1),
    ])
    , new Recipe('Bollo',
    'Descripcion2',
    'http://s3.amazonaws.com/gmi-digital-library/cc3c2b09-7e40-485e-af2c-fa1acc9f454a.jpg',
    [
      new Ingredient('Aceitunas', 1),
      new Ingredient('Pasas', 1),
      new Ingredient('Hojas', 1),
      new Ingredient('Hilo', 1),
    ])
  ];*/
  private recipes: Recipe[] = [];

  constructor(private slService: ShoppingListService) {}

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipes() {
      return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice());
  }
}
